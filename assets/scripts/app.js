/*jshint browser:true */
/*global $:false */
$(function() {
  'use strict';
  $('.style-switcher').not('#semantic-markup').click(function() {
      $('#less-style').attr('href', $(this).data('less-uri'));
      $('style').remove();
      return false;
  });
});
