Sourcetalk 2012 "Responsive Design" Demo Site
=============================================

## short description

WIP: Demo site for HTML5 Responsive Design.

http://st-html5-responsive.herokuapp.com

## longer

Responsive Web Design(RWD)

Styles

+ no styles
  + default web browser
  + reset
  + normalize
+ handmade
+ twitter bootstrap(TBS)
  + with semantic markup
  + use TBS classes

## directory structure

## meetform

## heroku
+ config.ru
+ Gemfile
+ Gemfile.lock

## license
